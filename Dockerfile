FROM node:10 as builder

WORKDIR       /app
COPY          docker-entrypoint.sh    /entrypoint.sh
COPY          src                     .
COPY          package.json            .

RUN           npm install
RUN           chmod +x /entrypoint.sh

EXPOSE        8080

ENTRYPOINT    [ "/entrypoint.sh" ]
CMD           [ "start" ]

# NGiNX
FROM nginx:1.15

EXPOSE 80

COPY nginx.conf /etc/nginx/nginx.conf
COPY html /usr/share/nginx/html
